//
//  Category.swift
//  Todoey
//
//  Created by Adrian Dróżdż on 08/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var colour : String = ""
    //FORWARD RELATIONSHIP
    //initialize empty List/Array that can contain Item objects
    let items = List<Item>()
}
