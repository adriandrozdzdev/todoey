//
//  Item.swift
//  Todoey
//
//  Created by Adrian Dróżdż on 08/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    @objc dynamic var dateCreated : Date?
    @objc dynamic var colour : String = ""
    //REVERSE RELATIONSHIP
    //Category.self => Create a Category.Type ~ PHPClass::class
    //property => name of forwardRelationship
    let parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
